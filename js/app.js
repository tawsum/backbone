(function() {

	window.App = {
		Models: {},
		Collections: {},
		Views: {},
		Router: {}
	};

	var vent = _.extend({}, Backbone.Events);

	App.Views.Appointment = Backbone.View.extend({
		initialize: function(){
			vent.on('appointment:show', this.show, this);
		},

		show: function(id) {
			console.log(id + '? Sure, Why not!');
		}
	});

	App.Router = Backbone.Router.extend({
		routes: {
			'': 'index',
			'appointment/:id': 'showAppointment'
		},

		index: function() {
			console.log('index page');
		},
		showAppointment: function(id) {
			vent.trigger('appointment:show', id);
		}
	});

	new App.Views.Appointment();

	new App.Router();
	Backbone.history.start();

})();